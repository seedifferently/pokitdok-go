pokitdok-go
===========


What?
-----

An experimental (and incomplete) PokitDok Platform API Client library for Go.


How?
----

1. Install [Go](https://golang.org/doc/install) (requires v1.8 or later).

2. Run `go get gitlab.com/seedifferently/pokitdok-go`

3. Add `pokitdok "gitlab.com/seedifferently/pokitdok-go"` to your imports.
