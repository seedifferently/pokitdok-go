package pokitdok

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// EligibilityService handles communication with the Eligibility API endpoint.
// See: https://platform.pokitdok.com/documentation/v4/#eligibility
type EligibilityService service

// Eligibility represents an eligibility service response.
// NOTE: incomplete representation
type Eligibility struct {
	Data struct {
		ValidRequest   bool   `json:"valid_request"`
		RejectReason   string `json:"reject_reason"`
		FollowUpAction string `json:"follow_up_action"`
		Coverage       struct {
			Active           bool     `json:"active"`
			ServiceTypes     []string `json:"service_types"`
			ServiceTypeCodes []string `json:"service_type_codes"`
			Coinsurance      []struct {
				BenefitPercent   float64  `json:"benefit_percent"`
				CoverageLevel    string   `json:"coverage_level"`
				InPlanNetwork    string   `json:"in_plan_network"`
				PlanDescription  string   `json:"plan_description"`
				ServiceTypeCodes []string `json:"service_type_codes"`
				ServiceTypes     []string `json:"service_types"`
			} `json:"coinsurance"`
			Copay []struct {
				Copayment struct {
					Amount   string `json:"amount"`
					Currency string `json:"currency"`
				} `json:"copayment"`
				CoverageLevel string `json:"coverage_level"`
				InPlanNetwork string `json:"in_plan_network"`
				Messages      []struct {
					Message string `json:"message"`
				} `json:"messages"`
				PlanDescription  string   `json:"plan_description"`
				ServiceTypeCodes []string `json:"service_type_codes"`
				ServiceTypes     []string `json:"service_types"`
			} `json:"copay"`
			Deductibles []struct {
				BenefitAmount struct {
					Amount   string `json:"amount"`
					Currency string `json:"currency"`
				} `json:"benefit_amount"`
				CoverageLevel   string `json:"coverage_level"`
				EligibilityDate string `json:"eligibility_date"`
				InPlanNetwork   string `json:"in_plan_network"`
				Messages        []struct {
					Message string `json:"message"`
				} `json:"messages"`
				PlanDescription  string   `json:"plan_description"`
				ServiceTypeCodes []string `json:"service_type_codes"`
				ServiceTypes     []string `json:"service_types"`
				TimePeriod       string   `json:"time_period"`
			} `json:"deductibles"`
		} `json:"coverage"`
		ServiceTypes     []string    `json:"service_types"`
		ServiceTypeCodes []string    `json:"service_type_codes"`
		Errors           interface{} `json:"errors"`
	}
	// Meta struct {}
}

// EligibilityOptions specifies the parameters sent to the eligibility service.
// NOTE: incomplete representation
type EligibilityOptions struct {
	Member struct {
		ID            string `json:"id,omitempty"`
		BirthDate     string `json:"birth_date,omitempty"`
		Gender        string `json:"gender,omitempty"`
		FirstName     string `json:"first_name,omitempty"`
		MiddleName    string `json:"middle_name,omitempty"`
		LastName      string `json:"last_name,omitempty"`
		Suffix        string `json:"suffix,omitempty"`
		PlanStartDate string `json:"plan_start_date,omitempty"`
		GroupNumber   string `json:"group_number,omitempty"`
	} `json:"member,omitempty"`
	ServiceTypes     []string `json:"service_types,omitempty"`
	TradingPartnerID string   `json:"trading_partner_id,omitempty"`
}

// Do sends a request to the service and returns its response.
func (s *EligibilityService) Do(opt *EligibilityOptions) (*Eligibility, error) {
	var e Eligibility
	url := s.client.BaseURL.String() + "/eligibility/"

	// Prepare the request data
	data := new(bytes.Buffer)
	err := json.NewEncoder(data).Encode(opt)
	if err != nil {
		return nil, err
	}

	// Perform the request
	resp, err := s.client.Post(url, mediaTypeJSON, data)
	if err != nil {
		return nil, err
	}

	// Check the status code (we ignore 422 here because response data errors
	// are handled later)
	if resp.StatusCode != 200 && resp.StatusCode != 422 {
		// For now, just return a simple error string about the unexpected status code
		return nil, fmt.Errorf("An unexpected response status code was returned: %d", resp.StatusCode)
	}

	// Parse the response data
	err = json.NewDecoder(resp.Body).Decode(&e)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Check for errors in the response data
	if e.Data.Errors != nil {
		// For now, just return a JSON representation of the errors data
		errors, _ := json.MarshalIndent(e.Data.Errors, "", "    ")
		return nil, fmt.Errorf("%v", string(errors))
	}

	return &e, nil
}
