package pokitdok

import (
	"net/http"
	"reflect"
	"testing"
)

func TestEligibilityService_Do(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/eligibility/", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "POST")
		testHeader(t, r, "Content-Type", mediaTypeJSON)

		w.WriteHeader(http.StatusOK)
		w.Write(eligibilityJSON)
	})

	got, err := client.Eligibility.Do(&EligibilityOptions{})
	if err != nil {
		t.Errorf("Eligibility.Do returned error: %v", err)
	}
	if want := eligibility; !reflect.DeepEqual(got, want) {
		t.Errorf("Eligibility.Do = %+v, want %+v", got, want)
	}
}

func TestEligibilityService_DoUnexpectedResponse(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/eligibility/", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "POST")
		testHeader(t, r, "Content-Type", mediaTypeJSON)

		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(http.StatusText(400)))
	})

	_, err := client.Eligibility.Do(&EligibilityOptions{})
	if err == nil {
		t.Error("Eligibility.DoUnexpectedResponse didn't return an error.")
	}
	if err.Error() != "An unexpected response status code was returned: 400" {

	}
	if want := "An unexpected response status code was returned: 400"; err.Error() != want {
		t.Errorf("Eligibility.DoUnexpectedResponse = %+v, want %+v", err.Error(), want)
	}
}

func TestEligibilityService_DoResponseDataErrors(t *testing.T) {
	setup()
	defer teardown()

	mux.HandleFunc("/eligibility/", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "POST")
		testHeader(t, r, "Content-Type", mediaTypeJSON)

		w.WriteHeader(http.StatusOK)
		w.Write(eligibilityErrorsJSON)
	})

	_, err := client.Eligibility.Do(&EligibilityOptions{})
	if err == nil {
		t.Error("Eligibility.DoResponseDataErrors didn't return an error.")
	}
	if want := eligibilityErrorsString; err.Error() != want {
		t.Errorf("Eligibility.DoResponseDataErrors = %+v, want %+v", err.Error(), want)
	}
}

var eligibilityJSON = []byte(`{
    "data": {
        "benefit_related_entities": [
            {
                "address": {
                    "address_lines": [
                        "PO Box 14079"
                    ],
                    "city": "Lexington",
                    "state": "KY",
                    "zipcode": "40512"
                },
                "benefit_amount": {
                    "amount": "0",
                    "currency": "USD"
                },
                "eligibility_or_benefit_information": "other_source_of_data",
                "entity_identifier_code": "payer",
                "entity_type": "organization",
                "organization_name": "Aetna"
            },
            {
                "benefit_amount": {
                    "amount": "0",
                    "currency": "USD"
                },
                "eligibility_or_benefit_information": "inactive",
                "entity_identifier_code": "payer",
                "entity_type": "organization",
                "organization_name": "HSA Open Access MC",
                "service_type_codes": [
                    "30"
                ],
                "service_types": [
                    "health_benefit_plan_coverage"
                ]
            }
        ],
        "client_id": "emkwGhlVpEmK74yUJqfE",
        "correlation_id": "d87f6c18-0a71-11e7-9b50-0242ac110003",
        "coverage": {
            "active": true,
            "coinsurance": [
                {
                    "benefit_percent": 0.0,
                    "coverage_level": "employee_and_spouse",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "GYN OFFICE VS"
                        },
                        {
                            "message": "GYN VISIT"
                        },
                        {
                            "message": "SPEC OFFICE"
                        },
                        {
                            "message": "SPEC VISIT"
                        },
                        {
                            "message": "PRIMARY OFFICE"
                        },
                        {
                            "message": "PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                },
                {
                    "benefit_percent": 0.5,
                    "coverage_level": "employee_and_spouse",
                    "in_plan_network": "no",
                    "messages": [
                        {
                            "message": "GYN OFFICE VS,COINS APPLIES TO OUT OF POCKET"
                        },
                        {
                            "message": "GYN VISIT,COINS APPLIES TO OUT OF POCKET"
                        },
                        {
                            "message": "SPEC OFFICE,COINS APPLIES TO OUT OF POCKET"
                        },
                        {
                            "message": "SPEC VISIT,COINS APPLIES TO OUT OF POCKET"
                        },
                        {
                            "message": "PRIME CARE VST,COINS APPLIES TO OUT OF POCKET"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                }
            ],
            "contacts": [
                {
                    "address": {
                        "address_lines": [
                            "PO Box 14079"
                        ],
                        "city": "Lexington",
                        "state": "KY",
                        "zipcode": "40512"
                    },
                    "contact_type": "payer",
                    "name": "Aetna"
                }
            ],
            "copay": [
                {
                    "copayment": {
                        "amount": "20",
                        "currency": "USD"
                    },
                    "coverage_level": "employee_and_spouse",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "PRIMARY OFFICE"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                },
                {
                    "copayment": {
                        "amount": "30",
                        "currency": "USD"
                    },
                    "coverage_level": "employee_and_spouse",
                    "in_plan_network": "not_applicable",
                    "messages": [
                        {
                            "message": "GYN OFFICE VS"
                        },
                        {
                            "message": "GYN VISIT"
                        },
                        {
                            "message": "SPEC OFFICE"
                        },
                        {
                            "message": "SPEC VISIT"
                        },
                        {
                            "message": "PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                }
            ],
            "deductibles": [
                {
                    "benefit_amount": {
                        "amount": "6000",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "eligibility_date": "2013-01-01",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "calendar_year"
                },
                {
                    "benefit_amount": {
                        "amount": "5956.09",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "INT MED AND RX"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "3000",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "eligibility_date": "2013-01-01",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "calendar_year"
                },
                {
                    "benefit_amount": {
                        "amount": "2983.57",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "yes",
                    "messages": [
                        {
                            "message": "INT MED AND RX"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "12000",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "eligibility_date": "2013-01-01",
                    "in_plan_network": "no",
                    "messages": [
                        {
                            "message": "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "calendar_year"
                },
                {
                    "benefit_amount": {
                        "amount": "11956.09",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "no",
                    "messages": [
                        {
                            "message": "INT MED AND RX"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "6000",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "eligibility_date": "2013-01-01",
                    "in_plan_network": "no",
                    "messages": [
                        {
                            "message": "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "calendar_year"
                },
                {
                    "benefit_amount": {
                        "amount": "5983.57",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "no",
                    "messages": [
                        {
                            "message": "INT MED AND RX"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                }
            ],
            "eligibility_begin_date": "2012-02-01",
            "group_description": "MOCK INDIVIDUAL ADVANTAGE PLAN",
            "group_number": "088818801000013",
            "insurance_type": "ppo",
            "level": "employee_and_spouse",
            "limitations": [
                {
                    "benefit_amount": {
                        "amount": "0",
                        "currency": "USD"
                    },
                    "coverage_level": "employee_and_spouse",
                    "messages": [
                        {
                            "message": "Unlimited Lifetime Benefits"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period_qualifier": "lifetime"
                },
                {
                    "benefit_amount": {
                        "amount": "0",
                        "currency": "USD"
                    },
                    "coverage_level": "employee_and_spouse",
                    "in_plan_network": "not_applicable",
                    "messages": [
                        {
                            "message": "Plan Requires PreCert"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                },
                {
                    "benefit_amount": {
                        "amount": "0",
                        "currency": "USD"
                    },
                    "coverage_level": "employee_and_spouse",
                    "messages": [
                        {
                            "message": "Commercial"
                        },
                        {
                            "message": "Plan includes NAP"
                        },
                        {
                            "message": "Pre-Existing may apply"
                        }
                    ],
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "98"
                    ],
                    "service_types": [
                        "professional_physician_visit_office"
                    ]
                }
            ],
            "out_of_pocket": [
                {
                    "benefit_amount": {
                        "amount": "3000",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "yes",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ]
                },
                {
                    "benefit_amount": {
                        "amount": "2983.57",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "yes",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "6000",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "yes",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ]
                },
                {
                    "benefit_amount": {
                        "amount": "5956.09",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "yes",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "12500",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "no",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ]
                },
                {
                    "benefit_amount": {
                        "amount": "12483.57",
                        "currency": "USD"
                    },
                    "coverage_level": "individual",
                    "in_plan_network": "no",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                },
                {
                    "benefit_amount": {
                        "amount": "25000",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "no",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ]
                },
                {
                    "benefit_amount": {
                        "amount": "24956.09",
                        "currency": "USD"
                    },
                    "coverage_level": "family",
                    "in_plan_network": "no",
                    "plan_description": "Open Choice",
                    "service_type_codes": [
                        "30"
                    ],
                    "service_types": [
                        "health_benefit_plan_coverage"
                    ],
                    "time_period": "remaining"
                }
            ],
            "plan_begin_date": "2013-02-15",
            "plan_description": "Open Choice",
            "plan_number": "0888188",
            "plans": [
                {
                    "group_description": "MOCK INDIVIDUAL ADVANTAGE PLAN",
                    "group_number": "088818801000013",
                    "insurance_type": "ppo",
                    "plan_description": "Open Choice",
                    "plan_number": "0888188"
                }
            ],
            "service_date": "2013-08-10",
            "service_type_codes": [
                "30",
                "98"
            ],
            "service_types": [
                "health_benefit_plan_coverage",
                "professional_physician_visit_office"
            ]
        },
        "originating_company_id": "1453915417",
        "payer": {
            "id": "MOCKPAYER",
            "name": "Aetna"
        },
        "pharmacy": {
            "is_eligible": false
        },
        "provider": {
            "first_name": "JEROME",
            "last_name": "AYA-AY",
            "npi": "1467560003"
        },
        "service_type_codes": [
            "30",
            "98"
        ],
        "service_types": [
            "health_benefit_plan_coverage",
            "professional_physician_visit_office"
        ],
        "subscriber": {
            "address": {
                "address_lines": [
                    "123 MAIN ST"
                ],
                "city": "SPARTANBURG",
                "state": "SC",
                "zipcode": "29307"
            },
            "birth_date": "1970-01-25",
            "first_name": "Jane",
            "gender": "unknown",
            "group_number": "088818801000013",
            "id": "W000000000",
            "last_name": "Doe"
        },
        "summary": {
            "deductible": {
                "family": {
                    "in_network": {
                        "applied": {
                            "amount": "43.91",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "6000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "5956.09",
                            "currency": "USD"
                        }
                    },
                    "out_of_network": {
                        "applied": {
                            "amount": "43.91",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "12000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "11956.09",
                            "currency": "USD"
                        }
                    }
                },
                "individual": {
                    "in_network": {
                        "applied": {
                            "amount": "16.43",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "3000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "2983.57",
                            "currency": "USD"
                        }
                    },
                    "out_of_network": {
                        "applied": {
                            "amount": "16.43",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "6000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "5983.57",
                            "currency": "USD"
                        }
                    }
                }
            },
            "out_of_pocket": {
                "family": {
                    "in_network": {
                        "applied": {
                            "amount": "43.91",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "6000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "5956.09",
                            "currency": "USD"
                        }
                    },
                    "out_of_network": {
                        "applied": {
                            "amount": "43.91",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "25000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "24956.09",
                            "currency": "USD"
                        }
                    }
                },
                "individual": {
                    "in_network": {
                        "applied": {
                            "amount": "16.43",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "3000",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "2983.57",
                            "currency": "USD"
                        }
                    },
                    "out_of_network": {
                        "applied": {
                            "amount": "16.43",
                            "currency": "USD"
                        },
                        "limit": {
                            "amount": "12500",
                            "currency": "USD"
                        },
                        "remaining": {
                            "amount": "12483.57",
                            "currency": "USD"
                        }
                    }
                }
            }
        },
        "trace_number": "1",
        "trading_partner_id": "MOCKPAYER",
        "valid_request": true
    },
    "meta": {
        "activity_id": "test",
        "application_mode": "test",
        "credits_billed": 1,
        "credits_remaining": 0,
        "processing_time": 0,
        "rate_limit_amount": 0,
        "rate_limit_cap": 5000,
        "rate_limit_reset": 9876543210
    }
}`)

var eligibilityErrorsJSON = []byte(`{
  "data": {
    "errors": {
      "validation": {
        "member": {
          "birth_date": [
            "Could not parse test. Should be ISO8601 (YYYY-MM-DD)."
          ]
        }
      }
    }
  },
  "meta": {
    "application_mode": "test",
    "processing_time": 0,
    "rate_limit_amount": 0,
    "rate_limit_cap": 5000,
    "rate_limit_reset": 9876543210
  }
}`)

var eligibilityErrorsString = `{
    "validation": {
        "member": {
            "birth_date": [
                "Could not parse test. Should be ISO8601 (YYYY-MM-DD)."
            ]
        }
    }
}`

var eligibility = &Eligibility{Data: struct {
	ValidRequest   bool   "json:\"valid_request\""
	RejectReason   string "json:\"reject_reason\""
	FollowUpAction string "json:\"follow_up_action\""
	Coverage       struct {
		Active           bool     "json:\"active\""
		ServiceTypes     []string "json:\"service_types\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		Coinsurance      []struct {
			BenefitPercent   float64  "json:\"benefit_percent\""
			CoverageLevel    string   "json:\"coverage_level\""
			InPlanNetwork    string   "json:\"in_plan_network\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
		} "json:\"coinsurance\""
		Copay []struct {
			Copayment struct {
				Amount   string "json:\"amount\""
				Currency string "json:\"currency\""
			} "json:\"copayment\""
			CoverageLevel string "json:\"coverage_level\""
			InPlanNetwork string "json:\"in_plan_network\""
			Messages      []struct {
				Message string "json:\"message\""
			} "json:\"messages\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
		} "json:\"copay\""
		Deductibles []struct {
			BenefitAmount struct {
				Amount   string "json:\"amount\""
				Currency string "json:\"currency\""
			} "json:\"benefit_amount\""
			CoverageLevel   string "json:\"coverage_level\""
			EligibilityDate string "json:\"eligibility_date\""
			InPlanNetwork   string "json:\"in_plan_network\""
			Messages        []struct {
				Message string "json:\"message\""
			} "json:\"messages\""
			PlanDescription  string   "json:\"plan_description\""
			ServiceTypeCodes []string "json:\"service_type_codes\""
			ServiceTypes     []string "json:\"service_types\""
			TimePeriod       string   "json:\"time_period\""
		} "json:\"deductibles\""
	} "json:\"coverage\""
	ServiceTypes     []string    "json:\"service_types\""
	ServiceTypeCodes []string    "json:\"service_type_codes\""
	Errors           interface{} "json:\"errors\""
}{ValidRequest: true, RejectReason: "", FollowUpAction: "", Coverage: struct {
	Active           bool     "json:\"active\""
	ServiceTypes     []string "json:\"service_types\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	Coinsurance      []struct {
		BenefitPercent   float64  "json:\"benefit_percent\""
		CoverageLevel    string   "json:\"coverage_level\""
		InPlanNetwork    string   "json:\"in_plan_network\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
	} "json:\"coinsurance\""
	Copay []struct {
		Copayment struct {
			Amount   string "json:\"amount\""
			Currency string "json:\"currency\""
		} "json:\"copayment\""
		CoverageLevel string "json:\"coverage_level\""
		InPlanNetwork string "json:\"in_plan_network\""
		Messages      []struct {
			Message string "json:\"message\""
		} "json:\"messages\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
	} "json:\"copay\""
	Deductibles []struct {
		BenefitAmount struct {
			Amount   string "json:\"amount\""
			Currency string "json:\"currency\""
		} "json:\"benefit_amount\""
		CoverageLevel   string "json:\"coverage_level\""
		EligibilityDate string "json:\"eligibility_date\""
		InPlanNetwork   string "json:\"in_plan_network\""
		Messages        []struct {
			Message string "json:\"message\""
		} "json:\"messages\""
		PlanDescription  string   "json:\"plan_description\""
		ServiceTypeCodes []string "json:\"service_type_codes\""
		ServiceTypes     []string "json:\"service_types\""
		TimePeriod       string   "json:\"time_period\""
	} "json:\"deductibles\""
}{Active: true, ServiceTypes: []string{"health_benefit_plan_coverage", "professional_physician_visit_office"}, ServiceTypeCodes: []string{"30", "98"}, Coinsurance: []struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{BenefitPercent: 0, CoverageLevel: "employee_and_spouse", InPlanNetwork: "yes", PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}, struct {
	BenefitPercent   float64  "json:\"benefit_percent\""
	CoverageLevel    string   "json:\"coverage_level\""
	InPlanNetwork    string   "json:\"in_plan_network\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{BenefitPercent: 0.5, CoverageLevel: "employee_and_spouse", InPlanNetwork: "no", PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}}, Copay: []struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{Copayment: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "20", Currency: "USD"}, CoverageLevel: "employee_and_spouse", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "PRIMARY OFFICE"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}, struct {
	Copayment struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"copayment\""
	CoverageLevel string "json:\"coverage_level\""
	InPlanNetwork string "json:\"in_plan_network\""
	Messages      []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
}{Copayment: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "30", Currency: "USD"}, CoverageLevel: "employee_and_spouse", InPlanNetwork: "not_applicable", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "GYN OFFICE VS"}, struct {
	Message string "json:\"message\""
}{Message: "GYN VISIT"}, struct {
	Message string "json:\"message\""
}{Message: "SPEC OFFICE"}, struct {
	Message string "json:\"message\""
}{Message: "SPEC VISIT"}, struct {
	Message string "json:\"message\""
}{Message: "PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"98"}, ServiceTypes: []string{"professional_physician_visit_office"}}}, Deductibles: []struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "6000", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "2013-01-01", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "5956.09", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "3000", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "2013-01-01", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIMARY OFFICE,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "2983.57", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "", InPlanNetwork: "yes", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "12000", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "2013-01-01", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "11956.09", Currency: "USD"}, CoverageLevel: "family", EligibilityDate: "", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "6000", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "2013-01-01", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX,GYN OFFICE VS,DED INCLUDED IN OOP,GYN VISIT,SPEC OFFICE,SPEC VISIT,PRIME CARE VST"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "calendar_year"}, struct {
	BenefitAmount struct {
		Amount   string "json:\"amount\""
		Currency string "json:\"currency\""
	} "json:\"benefit_amount\""
	CoverageLevel   string "json:\"coverage_level\""
	EligibilityDate string "json:\"eligibility_date\""
	InPlanNetwork   string "json:\"in_plan_network\""
	Messages        []struct {
		Message string "json:\"message\""
	} "json:\"messages\""
	PlanDescription  string   "json:\"plan_description\""
	ServiceTypeCodes []string "json:\"service_type_codes\""
	ServiceTypes     []string "json:\"service_types\""
	TimePeriod       string   "json:\"time_period\""
}{BenefitAmount: struct {
	Amount   string "json:\"amount\""
	Currency string "json:\"currency\""
}{Amount: "5983.57", Currency: "USD"}, CoverageLevel: "individual", EligibilityDate: "", InPlanNetwork: "no", Messages: []struct {
	Message string "json:\"message\""
}{struct {
	Message string "json:\"message\""
}{Message: "INT MED AND RX"}}, PlanDescription: "Open Choice", ServiceTypeCodes: []string{"30"}, ServiceTypes: []string{"health_benefit_plan_coverage"}, TimePeriod: "remaining"}}}, ServiceTypes: []string{"health_benefit_plan_coverage", "professional_physician_visit_office"}, ServiceTypeCodes: []string{"30", "98"}, Errors: interface{}(nil)}}
