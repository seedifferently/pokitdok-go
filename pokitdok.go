package pokitdok

import (
	"context"
	"io"
	"net/http"
	"net/url"

	"golang.org/x/oauth2/clientcredentials"
)

const (
	libraryVersion = "0.1.0"
	apiVersion     = "v4"
	tokenURL       = "https://platform.pokitdok.com/oauth2/token"
	defaultBaseURL = "https://platform.pokitdok.com/api/" + apiVersion
	mediaTypeJSON  = "application/json; charset=utf-8"
)

// APIClient manages communication with the API.
type APIClient struct {
	// HTTP client used to communicate with the API
	client *http.Client

	// Base URL for API requests
	BaseURL *url.URL

	// Services used for communicating with the API
	Eligibility *EligibilityService
}

type service struct {
	client *APIClient
}

// NewAPIClient returns a new API client.
func NewAPIClient(clientID, clientSecret string) *APIClient {
	// Initialize the oauth config
	config := clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     tokenURL,
	}

	// Initialize an HTTP client using the oauth config. This client will keep
	// track of the token's expiration, and auto-refresh if necessary.
	client := config.Client(context.Background())

	baseURL, _ := url.Parse(defaultBaseURL)

	c := &APIClient{client: client, BaseURL: baseURL}
	c.Eligibility = &EligibilityService{client: c}
	// ...others...

	return c
}

// Post issues a POST to the specified URL.
func (c *APIClient) Post(url string, contentType string, body io.Reader) (*http.Response, error) {
	return c.client.Post(url, contentType, body)
}
