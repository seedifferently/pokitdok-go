package pokitdok

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

var (
	// mux is the HTTP request multiplexer used with the test server
	mux *http.ServeMux

	// client is the Pokitdok API client being tested
	client *APIClient

	// server is a test HTTP server used to provide mock API responses
	server *httptest.Server
)

// Setup sets up a test HTTP server along with a pokitdok.APIClient that is
// configured to talk to that test server. Tests should register handlers on
// mux which provide mock responses for the API method being tested.
func setup() {
	// test server
	mux = http.NewServeMux()
	server = httptest.NewServer(mux)

	// client configured to use test server
	url, _ := url.Parse(server.URL)
	client = &APIClient{client: &http.Client{}, BaseURL: url}
	client.Eligibility = &EligibilityService{client: client}
}

// Teardown closes the test HTTP server.
func teardown() {
	server.Close()
}

func testMethod(t *testing.T, r *http.Request, want string) {
	if got := r.Method; got != want {
		t.Errorf("Request method: %v, want %v", got, want)
	}
}

func testHeader(t *testing.T, r *http.Request, header string, want string) {
	if got := r.Header.Get(header); got != want {
		t.Errorf("Header.Get(%q) returned %q, want %q", header, got, want)
	}
}
